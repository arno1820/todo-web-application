package ch.cern.todo.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ch.cern.todo.model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {
    Page<Task> findAll(Pageable pageable);

    Optional<Task> findById(Long id);

    @Query("SELECT t FROM Task t JOIN t.categories c WHERE c.id = :categoryId")
    Page<Task> findByCategoryId(Long categoryId, Pageable pageable);

}