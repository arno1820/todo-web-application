package ch.cern.todo.service;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ch.cern.todo.dto.TaskDto;
import ch.cern.todo.model.Category;
import ch.cern.todo.model.Task;
import ch.cern.todo.repository.TaskRepository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManagerFactory;

@Service
public class TaskService {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private CategoryService categoryService;

    private TaskDto toTaskDto(Task taskStub) {
        Session session = getSessionFactory().openSession();

        Task sessionTask = session.get(Task.class, taskStub.getId());
        Hibernate.initialize(sessionTask.getCategories());
        TaskDto result = TaskDto.fromTask(sessionTask);

        session.close();
        return result;

    }

    public Page<TaskDto> getAllTasks(Pageable pageable) {
        return taskRepository.findAll(pageable).map((Task t) -> toTaskDto(t));
    }

    public Optional<TaskDto> getTaskById(Long id) {
        return taskRepository.findById(id).map((Task t) -> toTaskDto(t));
    }

    public TaskDto saveTask(Task recievedTask) {
        return toTaskDto(taskRepository.save(recievedTask));
    }

    public TaskDto addCategory(Long taskId, Long categoryId) {
        Optional<TaskDto> possibleTask = getTaskById(taskId);
        if (possibleTask.isEmpty()) {
            throw new IllegalArgumentException(
                String.format("Task with ID %d does not exist", taskId));
        }
        Optional<Category> category = categoryService.getCategoryById(categoryId);
        if(category.isEmpty()){
            throw new IllegalArgumentException(
                String.format("Category with ID %d does not exist", categoryId));
        }

        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Task task = session.get(Task.class, taskId);
        Hibernate.initialize(task.getCategories());

        List<Category> categories = task.getCategories();
        categories.add(category.get());
        task.setCategories(categories);

        transaction.commit();
        session.close();
        return toTaskDto(task);
    }

    public Task updateTask(Long id, Task task) {
        task.setId(id);
        return taskRepository.save(task);
    }

    public Page<TaskDto> getTasksByCategoryId(Long categoryId, Pageable pageable) {
        return taskRepository.findByCategoryId(categoryId, pageable).map((Task t) -> toTaskDto(t));
    }

    private SessionFactory getSessionFactory() {
        return entityManagerFactory.unwrap(SessionFactory.class);
    }
}
