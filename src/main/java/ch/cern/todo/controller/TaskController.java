package ch.cern.todo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import ch.cern.todo.dto.TaskDto;
import ch.cern.todo.model.Task;
import ch.cern.todo.service.TaskService;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping
    public Page<TaskDto> getAllTasks(Pageable pageable) {
        return taskService.getAllTasks(pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskDto> getTaskById(@PathVariable Long id) {
        return taskService.getTaskById(id)
                .map(task -> ResponseEntity.ok().body(task))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    public TaskDto createTask(@RequestBody Task task) {
        return taskService.saveTask(task);
    }

    @PutMapping("/{task_id}/add-category/{category_id}")
    public ResponseEntity<Object> addCategory(@PathVariable Long task_id, @PathVariable Long category_id) {
        try {
            return ResponseEntity.ok().body(
                taskService.addCategory(task_id, category_id)
            );
        }
        catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PutMapping
    public ResponseEntity<Task> updateTask(@RequestBody Task task) {
        if (task.getId() != null) {
            return ResponseEntity.ok().body(
                taskService.updateTask(task.getId(), task));
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping("/{id}")
    public Task updateTask(@PathVariable Long id, @RequestBody Task task) {
        return taskService.updateTask(id, task);
    }

    @GetMapping("/category/{categoryId}")
    public Page<TaskDto> getTasksByCategoryId(@PathVariable Long categoryId, Pageable pageable) {
        return taskService.getTasksByCategoryId(categoryId, pageable);
    }

}