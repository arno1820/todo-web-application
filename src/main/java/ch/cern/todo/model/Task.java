package ch.cern.todo.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "TASK")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 100, nullable = false)
    private String name;

    @Column(length = 500)
    private String description;

    @Column(nullable = false)
    private Timestamp deadline;

    @ManyToMany
    @JoinTable(name = "TASK_CATEGORY",
    joinColumns = @JoinColumn(name = "task_id"))
    private List<Category> categories;

    public Long getId() { 
        return id; 
    }
    public void setId(Long id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }
    public Timestamp getDeadline() { return deadline; }
    public void setDeadline(Timestamp deadline) {this.deadline = deadline;}
    public List<Category> getCategories() {
        if (categories == null) return List.of();
        return categories;
    }
    public void setCategories(List<Category> categories) {this.categories = categories;}

}