package ch.cern.todo.dto;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

import ch.cern.todo.model.Category;
import ch.cern.todo.model.Task;

public class TaskDto {

    private Optional<Long> id;

    private String name;

    private String description;

    private Timestamp deadline;

    public List<Long> categoryIds;

    public Optional<Long> getId() { 
        return id; 
    }

    public TaskDto() {}
    public TaskDto(Long id, String name, String description, Timestamp deadline, List<Long> categoryIds) {
        this.id = Optional.of(id);
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.categoryIds = categoryIds;
    }

    public void setId(Long id) { this.id = Optional.of(id); }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }
    public Timestamp getDeadline() { return deadline; }
    public void setDeadline(Timestamp deadline) {this.deadline = deadline;}
    public void setCategoryIds(List<Long> categoryIds) {this.categoryIds = categoryIds;}

    public static TaskDto fromTask(Task task) {
        return new TaskDto(
            task.getId(),
            task.getName(),
            task.getDescription(),
            task.getDeadline(),
            task.getCategories().stream().map((Category c) -> c.getId()).toList()
        );
    }

}

