package ch.cern.todo.controller;

import ch.cern.todo.dto.TaskDto;
import ch.cern.todo.model.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testPostTask() {
        Task task = new Task();
        task.setName("Test Task");
        task.setDescription("This is a test task");

        ResponseEntity<TaskDto> response = restTemplate.postForEntity("/tasks", task, TaskDto.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Test Task", response.getBody().getName());
        assertEquals("This is a test task", response.getBody().getDescription());
    }

    @Test
    public void testPostTaskWithWrongFormat() {
        String task = "{This is a wrong format task";

        HttpEntity<String> entity = new HttpEntity<>(task);
        ResponseEntity<String> response = restTemplate.exchange("/tasks", HttpMethod.POST, entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

}